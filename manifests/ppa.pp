class puppet_workstation::ppa {

  include apt

  apt::ppa { 'ppa:nextcloud-devs/client': }
  apt::ppa { 'ppa:slgobinath/safeeyes': }
  apt::ppa { 'ppa:libreoffice/ppa': }

}