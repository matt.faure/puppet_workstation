class puppet_workstation::aliases {

  # Bash aliases for mfaure
  file { '/home/mfaure/.bash_aliases':
    ensure => file,
    owner => 'mfaure',
    group => 'mfaure',
    mode => '0644'
  }
  file_line { 'alias_ls_mfaure':
    path => '/home/mfaure/.bash_aliases',
    line => 'alias l="ls -al --color=auto"',
  }
  file_line { 'alias_mfaure':
    path => '/home/mfaure/.bash_aliases',
    line => 'source /home/mfaure/bin/my-aliases-MFAURE.sh',
  }

  # Bash aliases for root
  file { '/root/.bash_aliases':
    ensure => file,
    owner => 'root',
    group => 'root',
    mode => '0644'
  }
  file_line { 'alias_ls_root':
    path => '/root/.bash_aliases',
    line => 'alias l="ls -al --color=auto"',
  }

}