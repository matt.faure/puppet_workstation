class puppet_workstation::packages {

  # Packages are defined for Ubuntu 18.04

  # Packages from distro
  $package_list = [
    'amarok',
    'atop',
    'bash-completion',
    'borgbackup',
    'ccze',
    'curl',
    'compizconfig-settings-manager',
    'chromium-browser',
    'firefox',
    'hunspell',
    'hunspell-fr-comprehensive',
    'indicator-multiload',
    'keepassx',
    'nemo',
    'nemo-data',
    'nemo-fileroller',
    'net-tools',
    'network-manager-openvpn',
    'network-manager-openvpn-gnome',
    'redshift-gtk',
    'shutter',
    'sudo',
    'thunderbird',
    'ubuntu-unity-desktop',
    'vim',
    'whois',
    'zsh'
  ]
  $package_list_dev = [
    'docker.io',
    'git',
    'httpie',
    'maven',
    'openjdk-8-jdk',
  ]

  # Packages from PPA
  $package_list_from_PPA = [
    'nextcloud-client',
    'nextcloud-client-nemo',
    'safeeyes',
    'libreoffice',
  ]

  package { $package_list:
    ensure => 'present'
  }
  package { $package_list_dev:
    ensure => 'present'
  }

  Class['apt::update']
  ->
  package { $package_list_from_PPA:
    ensure => 'present'
  }

  # TDB
  # * IntelliJ:
  #   sudo snap install --stable --classic intellij-idea-ultimate

}