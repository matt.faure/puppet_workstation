#!/usr/bin/env bash

PUPPET6_DEB_FILENAME=puppet6-release-bionic.deb
MODULE_PATH=/root/.puppetlabs/etc/code/modules
PUPPET_CMD=/opt/puppetlabs/puppet/bin/puppet

wget "https://apt.puppetlabs.com/${PUPPET6_DEB_FILENAME}" -O "/tmp/${PUPPET6_DEB_FILENAME}"
dpkg -i "/tmp/${PUPPET6_DEB_FILENAME}"
apt update
apt install puppet-agent
"${PUPPET_CMD}" module install puppetlabs-stdlib --version 5.1.0 --modulepath=${MODULE_PATH}/
"${PUPPET_CMD}" module install puppetlabs-apt --version 6.2.1 --modulepath=${MODULE_PATH}/
"${PUPPET_CMD}" apply -e 'include puppet_workstation' --modulepath=${MODULE_PATH}/
